var express = require('express');
var router = express.Router();
const axios = require('axios');

router.get('/:q/:page', async (req, res) => {
  let bookResult = [];
  try {
    const bookList = await axios.get(`https://www.gramedia.com/api/products/?q=${req.params.q}&page=${req.params.page}&per_page=20`);

    for (x = 0; x < 20; x++) {
      console.log(bookList.data[x].href);
      const {data} = await axios.get(bookList.data[x].href);
      const bookDetail = data;

      const formatsReady = bookDetail.formats.length > 0;
      const authorsReady = bookDetail.authors.length > 0;

      bookResult.push({
        judul: bookDetail.name ? bookDetail.name : 'Tidak ada',
        pengarang: authorsReady ? bookDetail.authors[0].title : 'Tidak ada',
        penerbit: bookDetail.publisher ? bookDetail.publisher : 'Tidak ada',
        tahun: formatsReady ? bookDetail.formats[0].publishDate.slice(0,4) : 'Tidak ada',
        isbn: formatsReady ? bookDetail.formats[0].isbn : 'Tidak ada',
        harga: formatsReady ? bookDetail.formats[0].basePrice : 'Tidak ada'
      });
    }

    res.send(bookResult);
  } catch (e) {
    res.send(bookResult);
    console.log(e);
  }
});

module.exports = router;
